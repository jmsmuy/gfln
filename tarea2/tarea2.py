# -*- encoding: utf-8 -*-

#
#  Gramáticas Formales para el Lenguaje Natural
#  Grupo de PLN (InCo) - 2016
#
#  Entrega 2 - Treebanks, PCFG y Parsing
#
#  Este template define clases por sección y tiene metodos a ser completados.
#  Completar las secciones siguiendo su especificación y la letra de la entrega.
#
#
#  Grupo: 06
#
#  Estudiante 1: Alfonso Methol Diaz - 4667137-8
#  Estudiante 2: Guillermo Andres Lopez Salgado - 4257602-1
#  Estudiante 3: Juan Manuel San Martín - 4253963-9
#
#

import nltk
from nltk.parse import ViterbiParser
from nltk.tokenize import word_tokenize
import ancora
import pickle


# Parte 1 - Corpus
###################

class Corpus:
    """
    Clase de funcionalidades sobre el corpus AnCora.
    """

    def __init__(self, corpus_path):
        # Cargar corpus desde 'corpus_path'

        self.corpus = ancora.AncoraCorpusReader(corpus_path)
        self.pcfg = None
        self.pcfg_unk = None
        self.pcfglem = None
        self.pcfglemverb = None
        self.viterbi = None
        self.viterbi_unk = None

    ## Parte 1.1
    # a.
    def cant_oraciones(self):
        """
        Retorna la cantidad de oraciones del corpus.
        """
        return len(self.corpus.tagged_sents())

    # b.
    def oracion_mas_larga(self):
        """
        Retorna la oracion mas larga.
        (la primera si hay mas de una con el mismo largo)
        """
        largoMaximo = 0
        oracionMasLarga = ''
        sents = self.corpus.tagged_sents()
        cantSents = len(sents)

        for x in range(0, len(sents)):
            if (len(sents[x]) > largoMaximo):
                largoMaximo = len(sents[x])
                oracionMasLarga = sents[x]
                index = x

        oracionADevolver = ''
        for y in range(0,len(oracionMasLarga)):
            oracionADevolver = oracionADevolver + ' ' + (oracionMasLarga[y][1])

        return oracionADevolver

    # c.
    def largo_promedio_oracion(self):
        """
        Retorna el largo de oracion promedio.
        """
        acumulador = 0
        sents = self.corpus.tagged_sents()

        for x in range(0, len(sents)):
            acumulador = acumulador + len(sents[x])

        return acumulador / len(sents)

    # d.
    def palabras_frecs(self):
        """
        Retorna un diccionario (dict) palabra-frecuencia de las palabras del corpus.
        (considerar las palabras en minúsculas)
        """
        diccionario = {}
        words = self.corpus.tagged_words()
        contador = 0
        for x in range(0, len(words)):
            if words[x][1] is not None:
                word = words[x][1].lower()
                diccionario[word] = diccionario.get(word, 0) + 1

        return diccionario

    # e.
    def palabras_frecs_cat(self):
        diccionario = {}
        words = self.corpus.tagged_words()
        contador = 0
        for x in range(0, len(words)):
            if words[x][1] is not None:
                word = (words[x][1].lower(), words[x][0]['categ'])
                diccionario[word] = diccionario.get(word, 0) + 1

        return diccionario

    ## Parte 1.2
    # a
    def contar_nodos(self, tree):
        contador = 1
        if type(tree) == nltk.tree.Tree:
            for x in list(tree.__iter__()):
                contador = contador + self.contar_nodos(x)
        return contador

    def arbol_min_nodos(self):
        """
        Retorna el árbol del corpus con la mínima cantidad de nodos.
        (el primero si hay mas de uno con la misma cantidad)
        """

        minSent = 0
        minNodeCount = -1

        for sentence in list(self.corpus.parsed_sents_grammar().__iter__()):
            nodeCount = self.contar_nodos(sentence)
            if nodeCount < minNodeCount or minNodeCount == -1:
                minSent = sentence
                minNodeCount = nodeCount

        return minSent

    def arbol_max_nodos(self):
        """
        Retorna el árbol del corpus con la máxima cantidad de nodos.
        (el primero si hay mas de uno con la misma cantidad)
        """

        maxSent = 0
        maxNodeCount = -1

        for sentence in list(self.corpus.parsed_sents().__iter__()):
            nodeCount = self.contar_nodos(sentence)
            if nodeCount > maxNodeCount:
                maxSent = sentence
                maxNodeCount = nodeCount

        return maxSent

    # b
    def lema_en_arbol(self, node, lema):
        if type(node) == nltk.tree.Tree:
            for x in list(node.__iter__()):
                if self.lema_en_arbol(x, lema):
                    return True;
        else:
            return node['lem'] == lema
        return False

    def arboles_con_lema(self, lema):
        """
        Retorna todos los árboles que contengan alguna palabra con lema 'lema'.
        """

        trees = []

        for sentence in list(self.corpus.parsed_sents().__iter__()):
            if self.lema_en_arbol(sentence,lema):
                trees.append(sentence)

        return trees

    ## Parte 2.1
    # a b c
    def inferir_pcfg(self):
        if self.pcfg is None:
            prods = sum((t.productions() for t in self.corpus.parsed_sents_grammar()), [])

            S = nltk.Nonterminal('sentence')

            self.pcfg = nltk.induce_pcfg(S, prods)

        return self.pcfg

    def obtener_reglas_no_lexicas(self):
        rules = []

        grammar = self.inferir_pcfg()

        for rule in list(grammar.productions().__iter__()):
            if rule.is_nonlexical():
                rules.append(rule)

        return rules

    def obtener_categorias_lexicas(self):
        categorias = []

        grammar = self.inferir_pcfg()

        for rule in list(grammar.productions().__iter__()):
            categ = rule.lhs().symbol()[0:1]
            if rule.is_lexical() and categ not in categorias:
                categorias.append(categ)

        return categorias

    def obtener_reglas_lexicas_categoria(self, categ):
        rules = []

        grammar = self.inferir_pcfg()

        for rule in list(grammar.productions().__iter__()):
            if rule.is_lexical() and rule.lhs().symbol()[0:1] == categ:
                rules.append(rule)

        return rules

    ## Parte 2.2
    def obtener_viterbi_parser(self):
        if self.viterbi is None:
            self.viterbi = ViterbiParser(self.inferir_pcfg())

        return self.viterbi

    ## Parte 2.3
    def analizar_oracion(self, sentence):
        return self.obtener_viterbi_parser().parse(word_tokenize(sentence))

    ## Parte 3.1
    def inferir_pcfg_unk(self):
        if self.pcfg_unk is None:
            prods = sum((t.productions() for t in self.corpus.parsed_sents_grammar()), [])

            words = [x[0] for x in corpus.palabras_frecs().items() if x[1] == 1]

            produn = [x for x in prods if x.rhs().__len__() == 1 and x.rhs()[0] != None and type(x.rhs()[0]) != nltk.Nonterminal and x.rhs()[0].lower() in words]

            for prod in list(produn.__iter__()):
                prod._rhs = ('UNK',)

            S = nltk.Nonterminal('sentence')

            self.pcfg_unk = nltk.induce_pcfg(S, prods)

        return self.pcfg_unk

    ## Parte 3.2
    def obtener_viterbi_parser_unk(self):
        if self.viterbi_unk is None:
            self.viterbi_unk = ViterbiParser(self.inferir_pcfg_unk())

        return self.viterbi_unk

    def analizar_oracion_unk(self, sentence):
        words = word_tokenize(sentence)

        palabras_frecs = self.palabras_frecs()

        tokens = []

        for word in words:
            if palabras_frecs.get(word.lower(),0) > 1:
                tokens.append(word)
            else:
                tokens.append(u'UNK')

        return self.obtener_viterbi_parser_unk().parse(tokens)

    ## Parte 4.1
    # b
    def inferir_pcfg_lem(self):
        if self.pcfglem is None:
            prods = sum((t.productions() for t in self.corpus.parsed_sents_grammar_lem()), [])

            S = nltk.Nonterminal('sentence')

            self.pcfglem = nltk.induce_pcfg(S, prods)

        return self.pcfglem

    def obtener_viterbi_parser_lem(self):
        return ViterbiParser(self.inferir_pcfg_lem())

    def analizar_oracion_lem(self, sentence):
        return self.obtener_viterbi_parser_lem().parse(word_tokenize(sentence))

    ## Parte 4.2
    # b
    def obtenerProdsVerbLemas(self, prod, verbLemas):
        prodsVerb = []
        camposIzq = []
        camposDer = []
        existeOtroGV = False

        for i in range(0, len(prod.rhs())):
            campo = prod.rhs()[i]
            if type(campo) == nltk.Nonterminal and campo._symbol == 'grup.verb':

                for j in range(i+1, len(prod.rhs())):
                    campo = prod.rhs()[j]
                    camposDer.append(campo)
                    if type(campo) == nltk.Nonterminal and campo._symbol == 'grup.verb':
                        existeOtroGV = True

                for lema in verbLemas:
                    newProd = nltk.Production(prod._lhs, tuple(camposIzq + list(lema) + camposDer))
                    if existeOtroGV:
                        prodsVerb.Extend(self.obtenerProdsVerbLemas(newProd, lema))
                    else:
                        prodsVerb.append(newProd)
            else:
                camposIzq.append(campo)

        return prodsVerb

    def inferir_pcfg_lem_verb(self):
        if self.pcfglemverb is None:
            prods = sum((t.productions() for t in self.corpus.parsed_sents_grammar_lem_verb()), [])

            S = nltk.Nonterminal('sentence')

            self.pcfglemverb = nltk.induce_pcfg(S, prods)

        return self.pcfglemverb

    def obtener_viterbi_parser_lem_verb(self):
        return ViterbiParser(self.inferir_pcfg_lem_verb())

    def analizar_oracion_lem_verb(self, sentence):
        return self.obtener_viterbi_parser_lem_verb().parse(word_tokenize(sentence))


corpus = Corpus('ancora-3.0.1es/')

print('Parte 1.1\n')

print("Cantidad de oraciones: ")
print(corpus.cant_oraciones())

print("\nOración más larga: ")
print(corpus.oracion_mas_larga())

print("\nLargo promedio de oración: ")
print(corpus.largo_promedio_oracion())

print("\nFrecuencia de palabras: ")
print(corpus.palabras_frecs())

print("\nFrecuencia de palabras considerando la categoria: ")
print(corpus.palabras_frecs_cat())

print('Parte 1.2\n')

print("\nÁrbol con min cant nodos: ")
print(corpus.arbol_min_nodos())

print("\nÁrbol con max cant nodos: ")
print(corpus.arbol_max_nodos())

print("\nArboles con lema \'decir\': ")
print(corpus.arboles_con_lema('decir'))

corpus.inferir_pcfg()
# pickle.dump(corpus.pcfg, open("grammar.p", "wb"))
# corpus.pcfg = pickle.load( open( "grammar.p", "rb" ) )

corpus.inferir_pcfg_unk()
# pickle.dump(corpus.pcfg_unk, open("grammar_unk.p", "wb"))
# corpus.pcfg_unk = pickle.load(open( "grammar_unk.p", "rb" ) )

corpus.inferir_pcfg_lem()
# pickle.dump(corpus.pcfglem, open("grammar_lem.p", "wb"))
# corpus.pcfglem = pickle.load( open( "grammar_lem.p", "rb" ) )

corpus.inferir_pcfg_lem_verb()
# pickle.dump(corpus.pcfglemverb, open("grammar_lem_verb.p", "wb"))
# corpus.pcfglemverb = pickle.load( open( "grammar_lem_verb.p", "rb" ) )

print('Parte 2.3\n')
parse = corpus.analizar_oracion(u'El juez manifestó que las medidas exigidas por el gobierno actual son muy severas')
for t in parse:
    print(t)

parse = corpus.analizar_oracion(u'El partido entre los equipos europeos tendrá lugar este viernes')
for t in parse:
    print(t)

try:
    parse = corpus.analizar_oracion(u'El domingo próximo se presenta la nueva temporada de ópera')
    for t in parse:
        print(t)
except Exception as e:
    print(str(e))

print('Parte 3.3\n')
parse = corpus.analizar_oracion_unk(u'El domingo próximo se presenta la nueva temporada de ópera')
for t in parse:
    print(t)

parse = corpus.analizar_oracion_unk(u'Pedro y Juan jugarán el campeonato de fútbol')
for t in parse:
    print(t)

print('Parte 4.1 a\n')
parse = corpus.analizar_oracion(u'El juez vino que avión.')
for t in parse:
    print(t)

print('Parte 4.1 b\n')
parse = corpus.analizar_oracion_lem(u'El juez vino que avión.')
for t in parse:
    print(t)

print('Parte 4.2 a\n')
parse = corpus.analizar_oracion_lem(u'El juez manifestó su apoyo al gobierno.')
for t in parse:
    print(t)

parse = corpus.analizar_oracion_lem(u'El juez opinó su apoyo al gobierno.')
for t in parse:
    print(t)

print('Parte 4.2 b\n')
parse = corpus.analizar_oracion_lem_verb(u'El juez manifestó su apoyo al gobierno.')
for t in parse:
    print(t)

parse = corpus.analizar_oracion_lem_verb(u'El juez opinó su apoyo al gobierno.')
for t in parse:
    print(t)

print('Parte 4.2 c\n')
parse = corpus.analizar_oracion_lem(u'El juez manifestó que renunciará.')
for t in parse:
    print(t)

parse = corpus.analizar_oracion_lem_verb(u'El juez manifestó que renunciará.')
for t in parse:
    print(t)

