from nltk.corpus.reader.api import SyntaxCorpusReader
from nltk.corpus.reader import xmldocs
from nltk import tree
from nltk.util import LazyMap
from nltk.corpus.reader.util import concat


def parsedDict(element):
    if element:
        # element viewed as a list is non-empty (it has subelements)
        subtrees = map(parsedDict, element)
        subtrees = [t for t in subtrees if t is not None]
        return tree.Tree(element.tag, subtrees)
    else:
        # element viewed as a list is empty. we are in a terminal.
        if element.get('elliptic') == 'yes':
            return None
        else:
            word = {}
            word['wd'] = element.get('wd') or None
            word['categ'] = element.tag or None
            word['pos'] = element.get('pos') or None
            word['lem'] = element.get('lem') or None
            return tree.Tree(element.get('wd'), [word])

def parsedPos(element):
    if element:
        # element viewed as a list is non-empty (it has subelements)
        subtrees = map(parsedPos, element)
        subtrees = [t for t in subtrees if t is not None]
        return tree.Tree(element.tag, subtrees)
    else:
        # element viewed as a list is empty. we are in a terminal.
        if element.get('elliptic') == 'yes':
            return None
        else:
            return tree.Tree(element.get('pos') or 'unk', [element.get('wd')])

def parsedLem(element):
    if element:
        # element viewed as a list is non-empty (it has subelements)
        subtrees = map(parsedLem, element)
        subtrees = [t for t in subtrees if t is not None]
        return tree.Tree(element.tag, subtrees)
    else:
        # element viewed as a list is empty. we are in a terminal.
        if element.get('elliptic') == 'yes':
            return None
        else:
            return tree.Tree(element.get('lem') or 'unk', [element.get('wd')])

def parsedLemVerb(element):
    if element:
        # element viewed as a list is non-empty (it has subelements)
        subtrees = map(parsedLemVerb, element)
        subtrees = [t for t in subtrees if t is not None]
        if element.tag != 'grup.verb':
            return tree.Tree(element.tag, subtrees)
        else:
            if len(subtrees) == 1:
                return tree.Tree(subtrees[0]._label, subtrees)
            else:
                filtrado = [x._label for x in subtrees if x._label not in ('haber','ser','infinitiu', 'gerundi', 'a', 'que', 'de', 'sn', 'sadv', 'sp', 'S','grup.nom',';','"',',')]
                if len(filtrado) == 1:
                    tree.Tree(filtrado[0], subtrees)
                else:
                    filtrado = [x._label for x in subtrees if x._label not in ('sn', 'sp')]
                    if len(filtrado) > 0:
                        tree.Tree(filtrado[len(filtrado) - 1], subtrees)
                    else:
                        tree.Tree(subtrees[len(subtrees) - 1]._label, subtrees)
    else:
        # element viewed as a list is empty. we are in a terminal.
        if element.get('elliptic') == 'yes':
            return None
        else:
            return tree.Tree(element.get('lem') or 'unk', [element.get('wd')])

def tagged(element):
    # http://www.w3schools.com/xpath/xpath_syntax.asp
    # XXX: XPath '//*[@wd]' not working
    # return [(x.get('wd'), x.get('pos') or x.get('ne')) for x in element.findall('*//*[@wd]')] + [('.', 'fp')]
    return filter(lambda x: x != (None, None), parsedDict(element).pos())

def untagged(element):
    # http://www.w3schools.com/xpath/xpath_syntax.asp
    # XXX: XPath '//*[@wd]' not working
    # return [x.get('wd') for x in element.findall('*//*[@wd]')] + [('.', 'fp')]
    return filter(lambda x: x is not None, parsedDict(element).leaves())

class AncoraCorpusReader(SyntaxCorpusReader):
    # def __init__(self, xmlreader):
    #    self.xmlreader = xmlreader
    def __init__(self, path):
        self.xmlreader = xmldocs.XMLCorpusReader(path + '3LB-CAST', '.*\.xml')
        self.parsedsents = None
        self.parsedsentsgrammar = None
        self.parsedsentsgrammarlem = None
        self.parsedsentsgrammarlemverb = None
        self.taggedsents = None
        self.sentsvar = None
        self.taggedwords = None


    def parsed_sents(self, fileids=None):
        if self.parsedsents is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.parsedsents = LazyMap(parsedDict, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.parsedsents

    def parsed_sents_grammar(self, fileids=None):
        if self.parsedsentsgrammar is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.parsedsentsgrammar = LazyMap(parsedPos, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.parsedsentsgrammar

    def parsed_sents_grammar_lem(self, fileids=None):
        if self.parsedsentsgrammarlem is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.parsedsentsgrammarlem = LazyMap(parsedLem, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.parsedsentsgrammarlem

    def parsed_sents_grammar_lem_verb(self, fileids=None):
        if self.parsedsentsgrammarlemverb is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.parsedsentsgrammarlemverb = LazyMap(parsedLemVerb, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.parsedsentsgrammarlemverb

    def tagged_sents(self, fileids=None):
        if self.taggedsents is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.taggedsents =  LazyMap(tagged, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.taggedsents

    def sents(self, fileids=None):
        if self.sentsvar is None:
            if not fileids:
                fileids = self.xmlreader.fileids()
            self.sentsvar = LazyMap(untagged, concat([list(self.xmlreader.xml(fileid)) for fileid in fileids]))

        return self.sentsvar

    def tagged_words(self, fileids=None):
        if self.taggedwords is None:
            self.taggedwords = concat(self.tagged_sents(fileids))

        return self.taggedwords